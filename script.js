const buttonWrapperDiv = document.querySelector('.btn-wrapper');

const keyBoard = {
    Enter: 'Enter',
    s: 's',
    e: 'e',
    o: 'o',
    n: 'n',
    l: 'l',
    z: 'z',
}

let previousActiveBtn;

buttonWrapperDiv.addEventListener('click', (e) => {
    if(e.target !== e.currentTarget && e.target.classList.contains('btn')) {
        if(previousActiveBtn) previousActiveBtn.classList.remove('blue');
        e.target.classList.add('blue');
        previousActiveBtn = e.target;
    }
})

document.addEventListener('keydown', (e) => {
        if(keyBoard[`${e.key}`]) {
            if(previousActiveBtn) previousActiveBtn.classList.remove('blue');
            document.querySelector(`[data-key="${e.key}"]`).classList.add('blue');
            previousActiveBtn = document.querySelector(`[data-key="${e.key}"]`);
        }
})



